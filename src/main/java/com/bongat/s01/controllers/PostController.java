package com.bongat.s01.controllers;

import com.bongat.s01.models.Post;
import com.bongat.s01.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;

    // Create a new post
    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestBody Post newPost, @RequestHeader(value = "Authorization") String token) {
        postService.createPost(newPost, token);
        return new ResponseEntity<>("New post was created.", HttpStatus.CREATED);
    }

    // Retrieve all posts
    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPosts() {
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    // Retrieve all posts of a user
    @RequestMapping(value = "/posts/my-posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getUserPosts(@RequestHeader(value = "Authorization") String token) {
        return new ResponseEntity<>(postService.getUserPosts(token), HttpStatus.OK);
    }

    // Updating an existing post
    @RequestMapping(value = "/posts/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long id, @RequestBody Post updatedPost, @RequestHeader(value = "Authorization") String token) {
        postService.updatePost(id, updatedPost, token);
        return new ResponseEntity<>("An existing post was updated.", HttpStatus.OK);
    }

    // Deleting an existing post
    @RequestMapping(value = "/posts/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long id, @RequestHeader(value = "Authorization") String token) {
        postService.deletePost(id, token);
        return new ResponseEntity<>("An existing post was deleted.", HttpStatus.OK);
    }
}
