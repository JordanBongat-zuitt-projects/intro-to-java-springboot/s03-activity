package com.bongat.s01.controllers;

import com.bongat.s01.models.Comment;
import com.bongat.s01.models.Post;
import com.bongat.s01.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class CommentController {
    @Autowired
    CommentService commentService;

    // Create new comment
    @RequestMapping(value = "/comments/{postId}", method = RequestMethod.POST)
    public ResponseEntity<Object> createComment(@PathVariable Post post, @RequestBody String newComment, @RequestHeader(value = "Authorization") String token) {
        commentService.createComment(post, newComment, token);
        return new ResponseEntity<>("New comment was created", HttpStatus.CREATED);
    }

    // Update existing comment
    @RequestMapping(value = "/comments/{commentId}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateComment(@PathVariable Long commentId, @RequestBody String updatedComment, @RequestHeader(value = "Authorization") String token) {
        commentService.updateComment(commentId, updatedComment, token);
        return new ResponseEntity<>("An existing comment was updated", HttpStatus.OK);
    }

    // Delete existing comment
    @RequestMapping(value = "/comments/{commentId}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteComment(@PathVariable Long commentId, @RequestHeader(value = "Authorization") String token) {
        commentService.deleteComment(commentId, token);
        return new ResponseEntity<>("An existing comment was deleted", HttpStatus.OK);
    }

    // Get all comments
    @RequestMapping(value = "/comments", method = RequestMethod.GET)
    public ResponseEntity<Object> getComments() {
        return new ResponseEntity<>(commentService.getComments(), HttpStatus.OK);
    }

    // Get all comments of a user
    @RequestMapping(value = "/comments/my-comments", method = RequestMethod.GET)
    public ResponseEntity<Object> getUserComments(@RequestHeader(value = "Authorization") String token) {
        return new ResponseEntity<>(commentService.getUserComments(token), HttpStatus.OK);
    }
}
