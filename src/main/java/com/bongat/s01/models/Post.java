package com.bongat.s01.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="posts")
public class Post {
    // Properties
    // id
    @Id
    @GeneratedValue
    private Long id;

    // title
    @Column
    private String title;

    // content
    @Column
    private String content;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @OneToMany(mappedBy = "post")
    @JsonIgnore
    private Set<Comment> comments;

    // Constructor
    public Post() {}

    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }

    // Getters & Setters
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setUser(User author) {
        this.user = author;
    }

    public User getUser() {
        return user;
    }

    public Set<Comment> getComment() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }
}
