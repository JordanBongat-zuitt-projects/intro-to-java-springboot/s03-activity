package com.bongat.s01.exceptions;

public class UserException extends Exception {

    public UserException(String message) {
        super(message);
    }
}
