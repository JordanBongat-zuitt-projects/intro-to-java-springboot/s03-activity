package com.bongat.s01.services;

import com.bongat.s01.config.JwtToken;
import com.bongat.s01.models.Comment;
import com.bongat.s01.models.Post;
import com.bongat.s01.models.User;
import com.bongat.s01.repositories.CommentRepository;
import com.bongat.s01.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Set;


@Service
public class CommentServiceImplementation implements CommentService {
    @Autowired
    private CommentRepository commentRepo;
    @Autowired
    private UserRepository userRepo;
    @Autowired
    JwtToken jwtToken;

    public void createComment(Post post, String newComment, String token) {
        User user = userRepo.findByUsername(jwtToken.getUsernameFromToken(token));

        Comment newCommentObj = new Comment();
        newCommentObj.setPost(post);
        newCommentObj.setComment(newComment);
        newCommentObj.setUser(user);

        commentRepo.save(newCommentObj);
    }

    public ResponseEntity updateComment(Long id, String updatedComment, String token) {
        Comment existingComment = commentRepo.findById(id).get();
        String usernameComment = existingComment.getUser().getUsername();
        String authUsername = jwtToken.getUsernameFromToken(token);

        if (authUsername.equals(usernameComment)) {
            existingComment.setComment(updatedComment);
            commentRepo.save(existingComment);
            return new ResponseEntity<>("An existing comment was updated", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You're not authorized to update this post", HttpStatus.UNAUTHORIZED);
        }
    }

    public ResponseEntity deleteComment(Long id, String token) {
        Comment existingComment = commentRepo.findById(id).get();
        String usernameComment = existingComment.getUser().getUsername();
        String authUsername = jwtToken.getUsernameFromToken(token);

        if (authUsername.equals(usernameComment)) {
            commentRepo.deleteById(id);
            return new ResponseEntity<>("An existing post was deleted", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You're not authorized to delete this post", HttpStatus.UNAUTHORIZED);
        }
    }

    public Iterable<Comment> getComments() {
        return commentRepo.findAll();
    }

    public Set<Comment> getUserComments(String token) {
        User user = userRepo.findByUsername(jwtToken.getUsernameFromToken(token));

        if (user != null) {
            return user.getComments();
        }
        return null;
    }
}
