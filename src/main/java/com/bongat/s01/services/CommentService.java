package com.bongat.s01.services;

import com.bongat.s01.models.Comment;
import com.bongat.s01.models.Post;
import org.springframework.http.ResponseEntity;

import java.util.Set;

public interface CommentService {
    void createComment(Post post, String newComment, String token);
    ResponseEntity updateComment(Long id, String updatedComment, String token);
    ResponseEntity deleteComment(Long id, String token);
    Iterable<Comment> getComments();
    Set<Comment> getUserComments(String token);
}
