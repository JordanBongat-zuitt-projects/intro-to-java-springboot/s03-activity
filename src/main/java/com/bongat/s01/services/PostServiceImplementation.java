package com.bongat.s01.services;

import com.bongat.s01.config.JwtToken;
import com.bongat.s01.models.Post;
import com.bongat.s01.models.User;
import com.bongat.s01.repositories.PostRepository;
import com.bongat.s01.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class PostServiceImplementation implements PostService {
    @Autowired
    private PostRepository postRepo;
    @Autowired
    private UserRepository userRepo;
    @Autowired
    JwtToken jwtToken;

    public void createPost(Post newPost, String token) {
        // Retrieve an existing user object
        User author = userRepo.findByUsername(jwtToken.getUsernameFromToken(token));

        // Manually assign the new values to the properties
        Post newPostObj = new Post();
        newPostObj.setTitle(newPost.getTitle());
        newPostObj.setContent(newPost.getContent());
        newPostObj.setUser(author);

        postRepo.save(newPostObj);
    }

    public ResponseEntity updatePost(Long id, Post updatedPost, String token) {
        // Retrieve the post to be updated
        Post existingPost = postRepo.findById(id).get();
        String authorUsername = existingPost.getUser().getUsername();
        String authUsername = jwtToken.getUsernameFromToken(token);

        if (authUsername.equals(authorUsername)) {
            // Update the properties of the existing post
            existingPost.setTitle(updatedPost.getTitle());
            existingPost.setContent(updatedPost.getContent());
            // Save the updates
            postRepo.save(existingPost);
            return new ResponseEntity<>("An existing post was updated.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You're not authorized to update this post.", HttpStatus.UNAUTHORIZED);
        }
    }

    public ResponseEntity deletePost(Long id, String token) {
        // Retrieve the post to be updated
        Post existingPost = postRepo.findById(id).get();
        String authorUsername = existingPost.getUser().getUsername();
        String authUsername = jwtToken.getUsernameFromToken(token);

        if (authUsername.equals(authorUsername)) {
            postRepo.deleteById(id);
            return new ResponseEntity<>("An existing post was deleted.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You're not authorized to delete this post.", HttpStatus.UNAUTHORIZED);
        }
    }

    public Iterable<Post> getPosts() {
        return postRepo.findAll();
    }

    public Set<Post> getUserPosts(String token) {
        // Retrieve an existing user object
        User author = userRepo.findByUsername(jwtToken.getUsernameFromToken(token));

        // Return all the posts of this user
        return author.getPosts();
    }
}
